<?php

use App\Models\Client;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ClientController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $jumlahklien = Client::count();
    $jumlahklienpria = Client::where('jeniskelamin','pria')->count();
    $jumlahklienwanita = Client::where('jeniskelamin','wanita')->count();

    return view('welcome',compact('jumlahklien','jumlahklienpria','jumlahklienwanita'));
})->middleware('auth');

Route::get('/klien',[ClientController::class, 'index'])->name('klien')->middleware('auth');

Route::get('/tambahklien',[ClientController::class, 'tambahklien'])->name('tambahklien');
Route::post('/insertdata',[ClientController::class, 'insertdata'])->name('insertdata');

Route::get('/tampilkandata/{id}',[ClientController::class, 'tampilkandata'])->name('tampilkandata');
Route::post('/updatedata/{id}',[ClientController::class, 'updatedata'])->name('updatedata');

Route::get('/delete/{id}',[ClientController::class, 'delete'])->name('delete');
Route::get('/detail/{id}',[ClientController::class, 'detail'])->name('detail');

Route::get('/login',[LoginController::class, 'login'])->name('login');
Route::post('/loginloading',[LoginController::class, 'loginloading'])->name('loginloading');
Route::get('/register',[LoginController::class, 'register'])->name('register');
Route::post('/registeruser',[LoginController::class, 'registeruser'])->name('registeruser');
Route::get('/logout',[LoginController::class, 'logout'])->name('logout');



