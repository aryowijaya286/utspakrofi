<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index(){

        $data = Client::paginate(10);
        return view('dataklien',compact('data'));
    }

    public function tambahklien(){
        return view('tambahdata');
    }

    public function insertdata(Request $request){

        $data = Client::create($request->all());
        if($request->hasFile('foto')){
            $request->file('foto')->move('fotoklien/', $request->file('foto')->getClientOriginalName());
            $data->foto = $request->file('foto')->getClientOriginalName();
            $data->save();
        }
        return redirect()->route('klien')->with('success',' Data Berhasil Disimpan ');
    }

    public function tampilkandata($id){
        $data = Client::find($id);
        return view('tampildata', compact('data'));
    }

    public function updatedata(Request $request,$id){
        $data = Client::find($id);
        $data->update($request->all());
        return redirect()->route('klien')->with('success',' Data Berhasil Diupdate ');
    }

    public function delete($id){
        $data = Client::find($id);
        $data->delete();
        return redirect()->route('klien')->with('success',' Data Berhasil DiHapus ');
    }

    public function detail($id){
        $detail = Client::find($id);
        return view('detail',compact('detail'));
    }

}

